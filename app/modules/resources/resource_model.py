from google.appengine.ext import ndb

from core.ndb import BasicModel


class ResourceModel(BasicModel):
    eid = ndb.StringProperty(indexed=True)
    email = ndb.StringProperty(indexed=False)
    first_name = ndb.StringProperty(indexed=False)
    middle_name = ndb.StringProperty(indexed=False)
    last_name = ndb.StringProperty(indexed=False)
    role = ndb.StringProperty(indexed=True)
    org_level_dte = ndb.StringProperty(indexed=False)
    talent_segment = ndb.StringProperty(indexed=True)
    industry_specialty = ndb.StringProperty(indexed=False)
    tech_func_specialty = ndb.StringProperty(indexed=True)
    career_level = ndb.IntegerProperty(indexed=False)
    career_track = ndb.StringProperty(indexed=False)
    is_deleted = ndb.BooleanProperty(indexed=True, default=False)
    status = ndb.StringProperty(indexed=True)
    project = ndb.StringProperty(indexed=False)
    primary_skills = ndb.StringProperty(indexed=True)
    career_group = ndb.StringProperty(indexed=True)
    primary_skill_experience = ndb.IntegerProperty(indexed=True)
    location = ndb.StringProperty(indexed=True)

    @classmethod
    def get_all(cls):
        return cls.query().fetch()

    @classmethod
    def get_by_id(cls, url_safe):
        return ndb.Key(urlsafe=url_safe).get()

    @classmethod
    def create(cls, resource_data):
        res = cls()
        res.populate(**resource_data)
        res.put()
        return res

    @classmethod
    def update(cls, url_key, **updates):
        res = ndb.Key(urlsafe=url_key).get()

        if res is None or res.is_deleted:
            return

        res.populate(**updates)
        return res.put().get()
