import logging
from flask import Blueprint, abort, request
from core.utils import create_json_response
from app.modules.resources.resource_model import ResourceModel
import json
from google.net.proto.ProtocolBuffer import ProtocolBufferDecodeError
from collections import Counter

resources = Blueprint('resources', __name__, url_prefix='/api')

@resources.route('/resources_cursored', methods=['GET'])
def get_resources_cursor():
    
    params = {str(k) : str(v) for k, v in request.args.items()}
    cursor = params.pop('cursor', None)
    size = int(params.pop('size', None))
    sort = params.get('sort', 'resources_last_name')
    params.pop('sort')
    order = params.get('order', 'asc')
    params.pop('order')

    try:
        query_obj = ResourceModel.find_all_by_properties(is_deleted=False,**params)
    except InvalidQueryParamsException:
        logging.error(str(InvalidQueryParamsException('Invalid key in query params')))
        return InvalidQueryParamsException.message, InvalidQueryParamsException.status_code

    try:
        count = query_obj.count()
        data = {}
        if count > 0:
            if order == 'asc':
                query = query_obj.order(ResourceModel.properties[sort])
            else:
                query = query_obj.order(-ResourceModel.properties[sort])

        if cursor is not None and cursor is not '':
            data, cursor, more = query.fetch_page(size, start_cursor=Cursor(urlsafe=cursor))
        else:
            data, cursor, more = query.fetch_page(size)

        return create_json_response({'cursor': cursor, 'data': data, 'total_count': count})
    except Exception as ex:
        logging.error(str(ex))
        return str(ex), 500

@resources.route('/resources', methods=['GET'])
def index():
    """
	Supports optional query params for filtering:

		page:
			page number. defaults to 1

		size:
			number of objects returned by each page. defaults to 20

		sort:
			sort by attribute. defaults to resource last name

		order:
			order of results based on sort. accepts `asc` and `desc`.
			default to asc

		{attribute}:
			filters results to only return objects with an attribute
			matching the given value

	For example:

		/api/resources?page=1&sort=haboc&order=desc

	"""

    # Convert unicode key:value to str
    params = {str(k) : str(v) for k, v in request.args.items()}
    page = int(params.get('page', 1))
    size = int(params.get('size', 20))
    sort = params.get('sort', 'resources_last_name')
    params.pop('sort')
    order = params.get('order', 'asc')
    params.pop('order')
 

    try:
        query = ResourceModel.find_all_by_properties(is_deleted=False,**params)
    except InvalidQueryParamsException:
        logging.error(str(InvalidQueryParamsException('Invalid key in query params')))
        return InvalidQueryParamsException.message, InvalidQueryParamsException.status_code

    try:
        if order == 'asc':
            query = query.order(ResourceModel._properties[sort])
        else:
            query = query.order(-ResourceModel._properties[sort])


        return create_json_response({'data': query.fetch(limit=size, offset=(page - 1) * size), 'total_count': query.count()})
    except Exception as ex:
        logging.error(str(ex))
        return str(ex), 500

@resources.route('/resources/<url_safe>', methods=['GET'])
def get_resource(url_safe):
    """Display a certain ResourceModel entity.

    Args:
        url_key (str): URL-safe key of the entity to be displayed.

    Returns:
        JSON: ResourceModel
    """

    try:
        resource = ResourceModel.get_by_id(url_safe)
        if resource.is_deleted:
            return "Resource not found.", 404

        return create_json_response(resource)
    except ProtocolBufferDecodeError:
        logging.error("url_safe key {} not found".format(url_safe))
        return "url_safe key {} not found".format(url_safe), 404
    except Exception as ex:
        logging.error(str(ex))
        return str(ex), 500


@resources.route('/resources', methods=['POST'])
def create():
    """Create ResourceModel entity.

    Args:
        JSON: ResourceModel

    Returns:
        JSON: Created ResourceModel.
        int: HTTP status code.

    """
    try:
        return create_json_response(ResourceModel.create(request.get_json())), 200
    except Exception as ex:
        logging.error(str(ex))
        return str(ex), 500


@resources.route('/resources/<url_safe>', methods=['PUT'])
def update(url_safe):
    """Updates a certain ResourceModel entity.

    Args:
        url_key (str): URL-safe key of the entity to be updated.
    
    Returns:
        JSON: Updated list of the ResourceModel.
        int: HTTP status code.

    """
    try:
        updates = request.get_json()
        return create_json_response(ResourceModel.update(url_safe, **updates)), 200 if updates and len(updates) > 0 \
            else 204

    except Exception as ex:
        logging.error(str(ex))
        return str(ex), 500


@resources.route('/resources/<url_safe>', methods=['DELETE'])
def delete(url_safe):
    """Soft deletes a certain ResourceModel entity.

    Args:
        url_key (str): URL-safe key of the entity to be updated.

    Returns:
        JSON: Deleted ResourceModel object.
    """
    return create_json_response(ResourceModel.update(url_safe, is_deleted=True))


@resources.route('/resources/validate/<resource_id>', methods=['GET'])
def validate_resource_id(resource_id):
    try:
        return create_json_response(ResourceModel.validate_resource_id(resource_id))
    except Exception as ex:
        logging.error(str(ex))
        return str(ex), 500


@resources.route('/resources/dashboard', methods=['GET'])
def get_by_status():
    try:
        groups = ResourceModel.query().fetch(projection=[ResourceModel.career_group, ResourceModel.status])

        career_group_bench = []
        career_group_project = []
        in_bench_cntr = 0
        in_project_cntr = 0

        for group in groups:
            if group.status == 'in_bench':
                in_bench_cntr += 1
                career_group_bench.append(group.career_group)
            else:
                in_project_cntr += 1
                career_group_project.append(group.career_group)

        in_bench_by_groups = dict(Counter(career_group_bench))
        in_project_by_groups = dict(Counter(career_group_project))

        resp = {
            'status': {
                'in_bench': in_bench_cntr,
                'in_project': in_project_cntr
            },
            'groups': {
                'in_project': in_project_by_groups,
                'in_bench': in_bench_by_groups
            }
        }

        return create_json_response(resp)

    except Exception as e:
        logging.error('Encountered an error: {}'.format(e))
        return e, 500
