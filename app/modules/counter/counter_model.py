import logging
from app.exceptions import EntityNotFoundException, InvalidFormatException
from google.appengine.ext import ndb
from google.appengine.api.datastore_errors import BadValueError
from core.ndb import BasicModel


class CounterModel(BasicModel):
    MODULE_NAME = 'Client'

    module_name = ndb.StringProperty(indexed=True)
    counter_value = ndb.IntegerProperty(indexed=True, default=0)


    @classmethod
    def current_count(cls):
        counter = CounterModel.query(cls.module_name == cls.MODULE_NAME).get_async().get_result()

        if counter is None:
            counter = CounterModel(module_name=cls.MODULE_NAME).put().get_async().get_result()

        return counter.counter_value


    @classmethod
    def next_count(cls):
        counter = CounterModel.query(cls.module_name == cls.MODULE_NAME).get_async().get_result()

        if counter is None:
            counter = CounterModel(module_name=cls.MODULE_NAME)

        counter.counter_value += 1
        counter.put()

        return counter.counter_value


    @classmethod
    def reset_count(cls):
        counter = CounterModel.query(cls.module_name == cls.MODULE_NAME).get_async().get_result()

        if counter is None:
            counter = CounterModel(module_name=cls.MODULE_NAME)

        counter.counter_value = 0
        counter.put()

        return counter.counter_value