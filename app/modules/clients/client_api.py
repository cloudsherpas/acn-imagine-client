# [START of Imports]
from app.exceptions import (
    InvalidFormatException,
    InvalidQueryParamsException,
    ResourceNotFoundException
)
from client_model import ClientModel
from core.utils import create_json_response, handle_error
from flask import Blueprint, request

# [END of Imports]

clients = Blueprint('clients', __name__, url_prefix='/api/clients')


# [START of Validations]
"""@clients.before_request
def before_get():
    try:
        if request.method != 'GET':
            return None

        boolean = lambda b: (True if b == 'true' else
                             False if b == 'false' else b)

        available_args = {
            'page': int,
            'size': int,
            'sort': str,
            'order': str
        }
        available_args.update({
            key: int if isinstance(value, ndb.IntegerProperty) else
            float if isinstance(value, ndb.FloatProperty) else
            boolean if isinstance(value, ndb.BooleanProperty) else
            str
            for key, value in ClientModel._properties.iteritems()
        })
        available_args.pop('client_id')
        available_args.pop('client_name')

        req_args = {key: value for key, value in request.args.iteritems()}

        for key, value in req_args.iteritems():
            if key not in available_args:
                raise InvalidQueryParamsException(
                    "Invalid query ({}).".format(key))

            wrapper = available_args[key]
            req_args[key] = wrapper(value)

        request.args = req_args

    except Exception as error:
        handle_error(error)"""


@clients.before_request
def before_post():
    try:
        if request.method != 'POST':
            return None

        req_json = request.get_json()

        if req_json is None:
            return None

        include = ClientModel.__table__.columns
        exclude = ClientModel.EXCLUDED_ATTRIBUTES

        if not all(key not in exclude and key in include for key in req_json):
            raise InvalidFormatException(
                "Some fields cannot be edited.")

    except Exception as error:
        handle_error(error)


@clients.before_request
def before_put():
    try:
        if request.method != 'PUT':
            return None

        req_json = request.get_json()

        if req_json is None:
            return None

        include = ClientModel.__table__.columns
        exclude = ClientModel.EXCLUDED_ATTRIBUTES

        if not all(key not in exclude and key in include for key in req_json):
            raise InvalidFormatException(
                "Some fields cannot be edited.")

    except Exception as error:
        handle_error(error)


@clients.before_request
def before_delete():
    try:
        if request.method != 'DELETE':
            return None

    except Exception as error:
        handle_error(error)


# [END of Validations]

# [START of Controllers]
@clients.route("/init_table", methods=['GET'])
def init_table():
    try:
        ClientModel.create_table()
        return create_json_response("Done!")
    except Exception as error:
        handle_error(error)

@clients.route('', methods=['GET'])
def get_all():
    try:
        req_args = request.args

        properties = {key: value for key, value in req_args.iteritems()
                      if key in ClientModel.__table__.columns}
        properties['is_deleted'] = properties.get('is_deleted', False)
        response = {
            'data' : ClientModel.get_all(req_args, **properties),
            'total_count' : ClientModel.count(**properties)
        }

        return create_json_response(response)

    except Exception as error:
        handle_error(error)


@clients.route('', methods=['POST'])
def create():
    try:
        properties = request.get_json()
        return create_json_response(ClientModel.create_client(**properties))

    except Exception as error:
        handle_error(error)


@clients.route('/<string:url_safe>', methods=['GET'])
def get(url_safe):
    try:
        client = ClientModel.get_by_id(url_safe)
        return create_json_response(client)

    except Exception as error:
        handle_error(error)


@clients.route('/<string:url_safe>', methods=['PUT'])
def update(url_safe):
    try:
        updates = request.get_json()
        if 'is_deleted' in updates:
            del updates['is_deleted']

        response = create_json_response(ClientModel.update_client(url_safe, **updates))
        return (response, 200 if updates and len(updates) > 0 else 204)
    except Exception as error:
        handle_error(error)


@clients.route('/<string:url_safe>', methods=['DELETE'])
def delete(url_safe):
    try:
        return create_json_response(ClientModel.delete_client(url_safe))
    except Exception as error:
        handle_error(error)

@clients.route('/lookup', methods=['GET'])
def get_lookup():
    try:
        return create_json_response(ClientModel.get_lookup())

    except Exception as error:
        handle_error(error)
        # [END of Controllers]