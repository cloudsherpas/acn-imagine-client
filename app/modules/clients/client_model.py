import logging
from app.exceptions import EntityNotFoundException
from core.sqlalchemy import BaseModel, db


class ClientModel(BaseModel):
    __ddl__ = """
    CREATE TABLE `clients` (
      `meta_created_on` datetime DEFAULT NULL,
      `meta_updated_on` datetime DEFAULT NULL,
      `meta_created_by` varchar(255) DEFAULT NULL,
      `meta_updated_by` varchar(255) DEFAULT NULL,
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `client_id` varchar(255) DEFAULT NULL,
      `client_name` varchar(255) DEFAULT NULL,
      `client_type` varchar(255) DEFAULT NULL,
      `is_deleted`  tinyint(1) DEFAULT NULL,
      PRIMARY KEY (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
    """

    __tablename__ = 'clients'
    __singular_name__ = 'client'
    EXCLUDED_ATTRIBUTES = BaseModel.EXCLUDED_ATTRIBUTES + ['is_deleted']

    id = db.Column(db.Integer, primary_key=True)
    client_id = db.Column(db.String(255))
    client_name = db.Column(db.String(255))
    client_type = db.Column(db.String(255))
    is_deleted = db.Column(db.Boolean(), default=False)

    def __init__(self, client_name, client_type):
        # construct sensible default values here
        self.client_name = client_name
        self.client_type = client_type

    @classmethod
    def get_all(cls, request, **properties):
        page = request.get('page', 1)
        size = request.get('size', 100)
        sort = request.get('sort')
        order = request.get('order')
        return cls.query(page, size, sort, order, **properties)

    @classmethod
    def get_by_id(cls, client_id):
        client = cls.get_by(is_deleted=False, client_id=client_id)
        if client is None:
            raise EntityNotFoundException("No client with '{}' ID exists.".format(client_id))
        return client.to_dict()

    @classmethod
    def create_client(cls, **properties):
        client = cls(**properties).save()
        return client.to_dict()

    @classmethod
    def update_client(cls, client_id, **updates):
        client = cls.get_by(is_deleted=False, client_id=client_id)
        if client is None:
            raise EntityNotFoundException("No client with '{}' ID exists.".format(client_id))

        client.client_name = updates['client_name']
        client.client_type = updates['client_type']
        return client.update().to_dict()

    @classmethod
    def delete_client(cls, client_id):
        client = cls.get_by(is_deleted=False, client_id=client_id)
        if client is None:
            raise EntityNotFoundException("No client with '{}' ID exists.".format(client_id))
        client.is_deleted = True
        return client.update().to_dict()

    @classmethod
    def get_lookup(cls):
        return cls.query()