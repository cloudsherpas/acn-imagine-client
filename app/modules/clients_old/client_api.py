# [START of Imports]
from app.exceptions import (
    InvalidFormatException,
    InvalidQueryParamsException,
    ResourceNotFoundException
)
from app.modules.counter.counter_model import CounterModel
from client_model import ClientModel
from core.utils import create_json_response, handle_error
from flask import Blueprint, request
from google.appengine.ext import ndb
# [END of Imports]

clients = Blueprint('clients', __name__, url_prefix='/api/clients')

# [START of Validations]
@clients.before_request
def before_get():
    try:
        if request.method != 'GET':
            return None

        boolean = lambda b: (True if b == 'true' else
                             False if b == 'false' else b)

        available_args = {
            'page': int,
            'size': int,
            'sort': str,
            'order': str
        }
        available_args.update({
            key: int if isinstance(value, ndb.IntegerProperty) else
                 float if isinstance(value, ndb.FloatProperty) else
                 boolean if isinstance(value, ndb.BooleanProperty) else
                 str
            for key, value in ClientModel._properties.iteritems()
        })
        available_args.pop('client_id')
        available_args.pop('client_name')

        req_args = {key: value for key, value in request.args.iteritems()}

        for key, value in req_args.iteritems():
            if key not in available_args:
                raise InvalidQueryParamsException(
                    "Invalid query ({}).".format(key))

            wrapper = available_args[key]
            req_args[key] = wrapper(value)
            
        request.args = req_args

    except Exception as error:
        handle_error(error)


@clients.before_request
def before_post():
    try:
        if request.method != 'POST':
            return None

        req_body = request.get_json()
        
        if req_body is None:
            return None

        include = ClientModel._properties
        exclude = ['created', 'created_by', 'modified', 'modified_by',
                   'client_id', 'is_deleted']
        
        if not all(key not in exclude and key in include for key in req_body):
            raise InvalidFormatException(
                "Some fields cannot be edited.")

    except Exception as error:
        handle_error(error)


@clients.before_request
def before_put():
    try:
        if request.method != 'PUT':
            return None

        req_body = request.get_json()
        
        if req_body is None:
            return None

        include = ClientModel._properties
        exclude = ['created', 'created_by', 'modified', 'modified_by',
                   'client_id', 'is_deleted']
        
        if not all(key not in exclude and key in include for key in req_body):
            raise InvalidFormatException(
                "Some fields cannot be edited.")

    except Exception as error:
        handle_error(error)


@clients.before_request
def before_delete():
    try:
        if request.method != 'DELETE':
            return None

    except Exception as error:
        handle_error(error)
# [END of Validations]

# [START of Controllers]
@clients.route('', methods=['GET'])
def get_all():
    try:
        req_args = request.args

        properties = {key: value for key, value in req_args.iteritems()
                    if key in ClientModel._properties}
        properties['is_deleted'] = properties.get('is_deleted', False)
        query = ClientModel.find_all_by_properties(**properties)
        
        page = req_args.get('page', None)
        size = req_args.get('size', 20 if page else None)
        sort = ClientModel._properties[req_args.get('sort', 'created')]
        order = req_args.get('order', 'desc')

        query = query.order(sort if order == 'asc' else
                            -sort if order == 'desc' else None)
        
        return create_json_response({
            'data': (
                query.fetch(size, offset=((page - 1) * size)) if page else
                query.fetch(size) if size else
                query.fetch()
            ),
            'total_count': query.count()
        })

    except Exception as error:
        handle_error(error)


@clients.route('', methods=['POST'])
def create():
    try:
        req_body = request.get_json()

        properties = {key: value for key, value in req_body.iteritems()
                      if key in ClientModel._properties}
        properties['client_id'] = 'CLI-{}'.format(CounterModel.next_count())

        return create_json_response(ClientModel(**properties).put().get())

    except Exception as error:
        handle_error(error)


@clients.route('/<string:url_safe>', methods=['GET'])
def get(url_safe):
    try:
        entity = ndb.Key(urlsafe=url_safe).get()

        if entity is None or entity.is_deleted:
            raise ResourceNotFoundException(
                "No entity with url-safe '{}' exist.".format(url_safe))

        return create_json_response(entity)

    except Exception as error:
        handle_error(error)


@clients.route('/<string:url_safe>', methods=['PUT'])
def update(url_safe):
    try:
        entity = ndb.Key(urlsafe=url_safe).get()

        if entity is None or entity.is_deleted:
            raise ResourceNotFoundException(
                "No entity with url-safe '{}' exist.".format(url_safe))

        req_body = request.get_json()
        properties = {key: value for key, value in req_body.iteritems()
                      if key in ClientModel._properties}
        entity.populate(**properties)

        return create_json_response(entity.put().get())

    except Exception as error:
        handle_error(error)


@clients.route('/<string:url_safe>', methods=['DELETE'])
def delete(url_safe):
    try:
        entity = ndb.Key(urlsafe=url_safe).get()

        if entity is None or entity.is_deleted:
            raise ResourceNotFoundException(
                "No entity with url-safe '{}' exist.".format(url_safe))

        entity.is_deleted = True

        return create_json_response(entity.put().get())

    except Exception as error:
        handle_error(error)
# [END of Controllers]