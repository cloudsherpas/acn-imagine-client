# [START of Imports]
from core.utils import handle_error
from core.ndb import BasicModel
from google.appengine.ext import ndb
# [END of Imports]


class ClientModel(BasicModel):
    client_id = ndb.StringProperty(indexed=True, required=True)
    client_name = ndb.StringProperty()
    client_type = ndb.StringProperty(required=True)
    is_deleted = ndb.BooleanProperty(default=False, indexed=True)