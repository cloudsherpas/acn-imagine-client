environment="local"
cp $PWD/configurations/${environment}.yaml $PWD/env_variables.yaml

nosetests $PWD/tests/ --with-coverage --with-ferris --gae-sdk-path=$HOME/google-cloud-sdk/platform/google_appengine
(dev_appserver.py . --port=8085 --admin_port=8005 --storage_path .  --log_level debug)