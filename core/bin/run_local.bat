environment="local"
cp $PWD/configurations/${environment}.yaml $PWD/env_variables.yaml

nosetests $PWD/tests/ --with-coverage --with-ferris --gae-sdk-path=/c/'Program Files (x86)'/Google/'Cloud SDK'/google-cloud-sdk.staging/platform/google_appengine
(dev_appserver.py app.yaml --admin_port=9006 --port=8086 --log_level debug)