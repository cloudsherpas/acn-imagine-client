# [START of Imports]
import logging
import os
from app.exceptions import CustomException
from app.modules.clients.client_api import clients
from app.modules.resources.resource_api import resources
from core.auth import validate_authentication_header
from core.config import _is_app_spot
from core.config import _is_testbed
from core.cross_domain import crossdomain
from flask import (
    Flask,
    jsonify,
    request,
    send_from_directory
)
from flask_swagger import swagger
from flask_sqlalchemy import SQLAlchemy
from core.sqlalchemy import db
# [END of Imports]

app = Flask(__name__)
app.secret_key = 'super secret key'
app.config.update(DEBUG=(not _is_app_spot() or _is_testbed()))

# setup SQLAlchemy
if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine/'):
    app.config['SQLALCHEMY_DATABASE_URI'] = os.environ['SQLALCHEMY_DATABASE_URI']
else:
    app.config['SQLALCHEMY_DATABASE_URI'] = os.environ['SQLALCHEMY_DATABASE_LOCAL_URI']

app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db.init_app(app)

app.register_blueprint(clients)

@app.before_request
def auth_user():
    """
    executed before entering blueprint endpoints
    """

    logging.info(request.headers)
    validate_authentication_header(request)


@app.after_request
def add_header(response):
    """
    allow request from any source
    """
    response.headers["Access-Control-Allow-Origin"] = "*"
    return response


@app.errorhandler(404)
def page_not_found(e):
    """Return a custom 404 error."""
    return 'Sorry, nothing at this URL.', 404


@app.errorhandler(500)
def server_error(e):
    """Return a custom 500 error."""
    return 'Error while serving request', 500


@app.errorhandler(CustomException)
def handle_invalid_usage(error):
    """
    used to return a json formatted version of the error upon request
    :param error:
    :return:
    """
    logging.warn(error.message)
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response


@app.after_request
@crossdomain(origin="*", headers='content-type, google-id-token')
def after_request(response):
    return response


# api documentation
@app.route('/apidocs')
@app.route('/apidocs/<path:path>')
def openapi(path='index.html'):
    root_dir = os.path.dirname(os.path.realpath(__file__))
    return send_from_directory(os.path.join(root_dir, 'openapi', ), path)


@app.route("/apidocs/spec")
def spec():
    swag = swagger(app)
    swag['info']['version'] = "1.0"
    swag['info']['title'] = "Imagine App Resources"
    return jsonify(swag)